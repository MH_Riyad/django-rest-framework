from django.urls import path
from .views import PollListCreate, PollRetrieveUpdateDestroy

urlpatterns = [
    path("polls/", PollListCreate.as_view(), name="polls_list"),
    path("polls/<int:pk>/",PollRetrieveUpdateDestroy.as_view(), name="poll_detail"),
]